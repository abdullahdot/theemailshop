    
    <!-- Page Title Start -->
    <div id="pageTitle" class="HeaderAdjust" data-bg-src="img/background-img/page-title-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="section-title">
                        <h2>login</h2>
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Login Page</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->
    
    <!-- Login Area Start -->
    <div id="login">
        <div class="container">
            <form action="http://billing.ywhmcs.com/dologin.php?systpl=EcoHostingV1C1" method="post" id="loginForm">
                <div class="form-group">
                    <label for="loginEmail">Email address *</label>
                    <input type="email" name="username" class="form-control" id="loginEmail" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="loginPassword">Password *</label>
                    <input type="password" name="password" class="form-control" id="loginPassword" placeholder="Password">
                </div>
                <label for="loginRemember" class="checkbox"><input type="checkbox" name="rememberme" id="loginRemember"> Remember Me</label>
                <p class="help-block clearfix">
                    <button type="submit" class="btn submit-button">login</button>
                    <a href="http://billing.ywhmcs.com/pwreset.php?systpl=EcoHostingV1C1"><i class="fa fa-fw fa-key"></i>Forget Password ?</a>
                </p>
            </form>
        </div>
    </div>
    <!-- Login Area End -->