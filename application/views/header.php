<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The Email Shop</title>

    <!-- ====Favicons==== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.png" type="image/x-icon">
    
    
    <!-- ====Google Font Stylesheet==== -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- ====Font Awesome Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- ====jQuery UI Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/jquery-ui-custom.min.css" rel="stylesheet">
    
    <!-- ====Bootstrap Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- ====Animate Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/animate.min.css" rel="stylesheet">

    <!-- ====Owl Carousel Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/owl.carousel.css" rel="stylesheet">
    
    <!-- ====Main Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    
    <!-- ====Responsive Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/responsive-style.css" rel="stylesheet">
    
    <!-- ====Color Scheme Stylesheet==== -->
    <link href="<?php echo base_url()?>assets/css/colors/theme-color-8.css" rel="stylesheet" id="changeColorScheme">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="preloader--spinners">
            <div class="preloader--spinner preloader--spinner-1"></div>
            <div class="preloader--spinner preloader--spinner-2"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- Menu Area Start -->
    <div id="menu">
        <!-- Menu Topbar Start -->
        <div class="menu--topbar">
            <div class="container">
                <!-- Menu Topbar Contact Start -->
                <div class="menu-topbar--contact">
                    <ul class="nav navbar-nav">
                        <li><a href="tel:+4440000000"><i class="fa fa-phone"></i>+444 000 0000</a></li>
                        <li><a href="mailto:example@example.com"><i class="fa fa-envelope"></i>example@example.com</a></li>
                    </ul>
                </div>
                <!-- Menu Topbar Contact End -->
                <!-- Menu Topbar Social Start -->
                <ul class="menu-topbar--social nav navbar-nav navbar-right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
                <!-- Menu Topbar Social End -->
            </div>
        </div>
        <!-- Menu Topbar End -->
        <!-- Primary Menu Start -->
        <nav id="primaryMenu" class="navbar primary-menu-two">
            <div class="container">
                <!-- Logo Start -->
                <div class="primary--logo">
                    <h1><a href="<?php echo base_url()?>">
                            <img src="<?php echo base_url()?>assets/img/logo.png" class="logo" >
                    </h1>
                </div>
                <!-- Logo End -->
                <div class="primary--info clearfix">
                    <div class="primary--info-item">
                        <div class="primary--icon">
                            <img src="<?php echo base_url()?>assets/img/top-bar-icons/01.png" alt="" class="img-responsive">
                        </div>
                        <div class="primary--content">
                            <p class="count">24/7 Support</p>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="primary--info-item">
                        <div class="primary--icon">
                            <img src="<?php echo base_url()?>assets/img/top-bar-icons/02.png" alt="" class="img-responsive">
                        </div>
                        <div class="primary--content">
                            <p class="count">45 Day Guarantee</p>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="primary--info-item">
                        <div class="primary--icon">
                            <img src="<?php echo base_url()?>assets/img/top-bar-icons/03.png" alt="" class="img-responsive">
                        </div>
                        <div class="primary--content">
                            <p class="count">99.9% Uptime</p>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Primary Menu End -->

        <!-- Secondary Menu Start -->
        <nav id="secondaryMenu" class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#secondaryNavbar" aria-expanded="false" aria-controls="secondaryNavbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="login-btn hidden-lg hidden-md hidden-sm">
                        <!--<a href="login.html" class="btn">Client Area</a>-->
                        <a href="#" class="btn">Client Area</a>
                    </div>
                </div>
                <!-- Secondary Menu Links Start -->
                <div id="secondaryNavbar" class="reset-padding navbar-collapse collapse">
                    <ul class="secondary-menu-links nav navbar-nav">
                        <li class="dropdown active">
                            <a href="#" data-toggle="dropdown">Home<span class="caret"></span></a>
<!--                            <ul class="dropdown-menu">
                                <li class="active"><a href="../version-1/index.html">Home Version 1</a></li>
                                <li><a href="../version-2/index.html">Home Version 2</a></li>
                                <li><a href="../version-3/index.html">Home Version 3</a></li>
                            </ul>-->
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown">Hosting<span class="caret"></span></a>
<!--                            <ul class="dropdown-menu">
                                <li><a href="shared-hosting.html">Shared Hosting</a></li>
                                <li><a href="reseller-hosting.html">Reseller Hosting</a></li>
                                <li><a href="vps-hosting.html">VPS Hosting</a></li>
                                <li><a href="dedicated-hosting.html">Dedicated Hosting</a></li>
                            </ul>-->
                        </li>
                        <li><a href="#">Domains</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown">Pages<span class="caret"></span></a>
<!--                            <ul class="dropdown-menu">
                                <li><a href="about.html">About</a></li>
                                <li><a href="faq.html">FAQ</a></li>
                                <li><a href="testimonial.html">Testimonial</a></li>
                                <li><a href="login.html">Login</a></li>
                                <li><a href="affiliate.html">Affiliate</a></li>
                                <li><a href="404.html">404</a></li>
                            </ul>-->
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown">Blog<span class="caret"></span></a>
<!--                            <ul class="dropdown-menu">
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="blog_sidebar-left.html">Blog Details Left</a></li>
                                <li><a href="blog_sidebar-right.html">Blog Details Right</a></li>
                            </ul>-->
                        </li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                    <ul class="secondary-menu-links nav navbar-nav navbar-right hidden-xs">
                        <li><a href="login.html" class="btn">Client Area</a></li>
                    </ul>
                </div>
                <!-- Secondary Menu Links End -->
            </div>
        </nav>
        <!-- Secondary Menu End -->
    </div>
    <!-- Menu Area End -->