    
    <!-- Footer Area Start -->
    <div id="footer">
        <div class="container">
            <div class="row">
                <!-- Footer About Widget Start -->
                <div class="col-md-6 footer-widget">
                    <div class="footer-about">
                        <!-- Footer Title Start -->
                        <h4>About Us</h4>
                        <!-- Footer Title End -->
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum quod mollitia quisquam. Architecto quam in atque sint voluptatem, consequatur consectetur ab ipsum maxime quod consequuntur excepturi illum dolorem ex modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur animi id ex perspiciatis distinctio sequi minima. Velit inventore fugit, quisquam molestias nesciunt dolorem reprehenderit temporibus unde, cupiditate pariatur libero dolorum!</p>
                        <a href="about.html" class="btn btn-custom-reverse">Read More</a>
                    </div>
                </div>
                <!-- Footer About Widget End -->
                <!-- Footer Subscribe Widget Start -->
                <div class="col-md-3 col-sm-6 footer-widget">
                    <!-- Footer Title Start -->
                    <h4>Subscribe</h4>
                    <!-- Footer Title End -->
                    <!-- Subscribe Form Start -->
                    <form action="http://themelooks.us12.list-manage.com/subscribe/post?u=50e1e21235cbd751ab4c1ebaa&amp;id=ac81d988e4" method="post" name="mc-embedded-subscribe-form" target="_blank" id="subscribeForm" novalidate="novalidate">
                        <input type="text" name="MERGE1" placeholder="Enter your full name" class="input-box">
                        <input type="text" name="EMAIL" placeholder="Enter your email address" class="input-box">
                        <input type="submit" value="Subscribe" class="submit-button">
                    </form>
                    <!-- Subscribe Form End -->
                </div>
                <!-- Footer Subscribe Widget End -->
                <!-- Footer Widget Start -->
                <div class="col-md-3 col-sm-6 footer-widget">
                    <!-- Footer Title Start -->
                    <h4>Company</h4>
                    <!-- Footer Title End -->
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Acceptable Usage Policy</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="#">DMCA Policy</a></li>
                    </ul>
                </div>
                <!-- Footer Widget End -->
            </div>
        </div>
        <div class="contact-info">
            <div class="container">
                <div class="row">
                    <!-- Contact Info Item Start -->
                    <div class="col-md-3 col-xs-6">
                        <i class="fa fa-headphones"></i>
                        <a href="#">24/7 Customer Support</a>
                    </div>
                    <!-- Contact Info Item End -->
                    <!-- Contact Info Item Start -->
                    <div class="col-md-3 col-xs-6">
                        <i class="fa fa-envelope-o"></i>
                        <a href="#">support@example.com</a>
                    </div>
                    <!-- Contact Info Item End -->
                    <!-- Contact Info Item Start -->
                    <div class="col-md-3 col-xs-6">
                        <i class="fa fa-comments-o"></i>                    
                        <a href="#">Click Here To Live Chat</a>
                    </div>
                    <!-- Contact Info Item End -->
                    <!-- Contact Info Item Start -->
                    <div class="col-md-3 col-xs-6">
                        <i class="fa fa-phone"></i>
                        <a href="#">+44 000 000 000</a>
                    </div>
                    <!-- Contact Info Item End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Area End -->
    
    <!-- Copyright Area Start -->
    <div id="copyright">
        <div class="container">
            <!-- Copyright Text Start -->
            <p class="left">Copyright 2018 &copy; <a href="<?php echo base_url()?>">The Email Shop</a>. All Rights Reserved.</p>
            <!-- Copyright Text End -->
            <p class="right">We Accept: <img src="<?php echo base_url()?>assets/img/payment-methods.png" alt=""></p>
        </div>
    </div>
    <!-- Copyright Area End -->
    
    <!-- Back To Top Button Start -->
    <div id="backToTop">
        <a href="#top"><i class="fa fa-angle-up"></i></a>
    </div>
    <!-- Back To Top Button Start -->

    <!-- ====jQuery Library==== -->
    <script src="<?php echo base_url()?>assets/js/jquery-3.1.0.min.js"></script>

    <!-- ====jQuery UI Library==== -->
    <script src="<?php echo base_url()?>assets/js/jquery-ui-custom.min.js"></script>

    <!-- ====Bootstrap Library==== -->
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

    <!-- ====jQuery Validation Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

    <!-- ====Owl Carousel Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/owl.carousel.min.js"></script>

    <!-- ====jQuery Waypoints Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/jquery.waypoints.min.js"></script>

    <!-- ====jQuery Counter Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/jquery.counterup.min.js"></script>
    
    <!-- ====jQuery UI Touch Punch Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.min.js"></script>
    
    <!-- ====RetinaJS Plugin==== -->
    <script src="<?php echo base_url()?>assets/js/retina.min.js"></script>

    <!-- ====Main Script==== -->
    <script src="<?php echo base_url()?>assets/js/main.js"></script>
    
</body>
</html>
