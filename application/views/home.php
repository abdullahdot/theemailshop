    <!-- Header Area Start -->
    <div id="header" data-bg-src="<?php echo base_url()?>assets/img/background-img/slider-bg-1.jpg" class="HeaderAdjust">
        <!-- Header Slider Start -->
        <div class="header-slider">
            <div class="header-slider--item header-slider--item-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 header-item-content-holder">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <!-- Header Slider Content Start -->
                                    <div class="header-item-content">
                                        <h2>Shared Hosting</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis dolores ducimus pariatur optio sint autem odio.</p>
                                        <div class="list clearfix">
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                        </div>
                                        <div class="price">
                                            <a href="#" class="btn btn-lg btn-custom-reverse">View Details</a>
                                        </div>
                                    </div>
                                    <!-- Header Slider Content End -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 header-item-img">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <figure class="clearfix">
                                        <img src="<?php echo base_url()?>assets/img/header-slider-img/01.png" alt="" class="img-responsive owl-fadeInUp">
                                        <figcaption>
                                            <div class="header-item-badge header-item-badge-1 owl-fadeInLeft">
                                                <p>Starting at<span>$4.99</span>/year</p>
                                            </div>
                                            <div class="header-item-badge header-item-badge-2 owl-fadeInRight">
                                                <p>Up To<span>10%</span>OFF</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-slider--item header-slider--item-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 header-item-content-holder">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <!-- Header Slider Content Start -->
                                    <div class="header-item-content">
                                        <h2>Reseller Hosting</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis dolores ducimus pariatur optio sint autem odio.</p>
                                        <div class="list clearfix">
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                        </div>
                                        <div class="price">
                                            <a href="#" class="btn btn-lg btn-custom-reverse">View Details</a>
                                        </div>
                                    </div>
                                    <!-- Header Slider Content End -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 header-item-img">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <figure class="clearfix">
                                        <img src="<?php echo base_url()?>assets/img/header-slider-img/02.png" alt="" class="img-responsive owl-fadeInUp">
                                        <figcaption>
                                            <div class="header-item-badge header-item-badge-1 owl-fadeInLeft">
                                                <p>Starting at<span>$9.99</span>/year</p>
                                            </div>
                                            <div class="header-item-badge header-item-badge-2 owl-fadeInRight">
                                                <p>Up To<span>10%</span>OFF</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-slider--item header-slider--item-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 header-item-content-holder">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <!-- Header Slider Content Start -->
                                    <div class="header-item-content">
                                        <h2>VPS Hosting</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis dolores ducimus pariatur optio sint autem odio.</p>
                                        <div class="list clearfix">
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                        </div>
                                        <div class="price">
                                            <a href="#" class="btn btn-lg btn-custom-reverse">View Details</a>
                                        </div>
                                    </div>
                                    <!-- Header Slider Content End -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 header-item-img">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <figure class="clearfix">
                                        <img src="<?php echo base_url()?>assets/img/header-slider-img/03.png" alt="" class="img-responsive owl-fadeInUp">
                                        <figcaption>
                                            <div class="header-item-badge header-item-badge-1 owl-fadeInLeft">
                                                <p>Starting at<span>$49.99</span>/year</p>
                                            </div>
                                            <div class="header-item-badge header-item-badge-2 owl-fadeInRight">
                                                <p>Up To<span>10%</span>OFF</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-slider--item header-slider--item-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 header-item-content-holder">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <!-- Header Slider Content Start -->
                                    <div class="header-item-content">
                                        <h2>Dedicated Server</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis dolores ducimus pariatur optio sint autem odio.</p>
                                        <div class="list clearfix">
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                            <p><i class="fa fa-check"></i>Lorem ipsum dolor sit</p>
                                        </div>
                                        <div class="price">
                                            <a href="#" class="btn btn-lg btn-custom-reverse">View Details</a>
                                        </div>
                                    </div>
                                    <!-- Header Slider Content End -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 header-item-img">
                            <div class="vc-parent">
                                <div class="vc-child">
                                    <figure class="clearfix">
                                        <img src="<?php echo base_url()?>assets/img/header-slider-img/04.png" alt="" class="img-responsive owl-fadeInUp">
                                        <figcaption>
                                            <div class="header-item-badge header-item-badge-1 owl-fadeInLeft">
                                                <p>Starting at<span>$99.99</span>/year</p>
                                            </div>
                                            <div class="header-item-badge header-item-badge-2 owl-fadeInRight">
                                                <p>Up To<span>10%</span>OFF</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Slider End -->
        <!-- Header Slider Nav Start -->
        <div class="header--slider-nav">
            <div class="container">
                <ul>
                    <li class="active"><p><i class="fa fa-laptop"></i><span>Shared Hosting</span></p></li>
                    <li><p><i class="fa fa-hdd-o"></i><span>Reseller Hosting</span></p></li>
                    <li><p><i class="fa fa-cloud"></i><span>VPS Hosting</span></p></li>
                    <li><p><i class="fa fa-server"></i><span>Dedicated Server</span></p></li>
                </ul>
            </div>
        </div>
        <!-- Header Slider Nav End -->
    </div>
    <!-- Header Area End -->
    
    <!-- Domain Search Area Start -->
    <div id="offer">
        <div class="container">
            <div class="row content">
                <div class="col-md-3 left-content">
                    <p><span>Looking a Premium Quality</span> Domain Name?</p>
                </div>
                <div class="col-md-9 right-content">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- Offer Form Start -->
                            <form action="http://billing.ywhmcs.com/domainchecker.php?systpl=EcoHostingV1C1" method="post" id="domainSearchForm">
                                <div class="row reset-gutter">
                                    <div class="col-sm-6">
                                        <input class="form-control" name="domain" type="text" placeholder="Enter your domain">
                                    </div>
                                    <div class="col-sm-3 select-box">
                                        <select class="form-control" name="ext">
                                            <option>.com</option>
                                            <option>.net</option>
                                            <option>.org</option>
                                            <option>.info</option>
                                            <option>.us</option>
                                            <option>.eu</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn submit-button-custom" type="submit">SEARCH</button>
                                    </div>
                                </div>
                            </form>
                            <!-- Offer Form End -->
                            <div class="domain--offer">
                                <span>Get 10% Off Today To Register A Domain</span>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="row domain-ext hidden-xs">
                                <div class="col-sm-4">
                                    <div class="extension">
                                        <span class="name">.com</span>
                                        <span>$3.99/y</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="extension">
                                        <span class="name">.net</span>
                                        <span>$3.99/y</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="extension">
                                        <span class="name">.org</span>
                                        <span>$3.99/y</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Domain Search Area End -->
    
    <!-- Features Area Start -->
    <div id="features">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section-title">
                <h2>why choose us</h2>
            </div>
            <!-- Section Title End -->
            <div class="row vc--features">
                <div class="col-md-4 features--img">
                    <img src="<?php echo base_url()?>assets/img/features-img/cloudHosting.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/01.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Firewall</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/02.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Data Encryption</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/03.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Data Analysis</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/04.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Data Protection</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/05.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Support Center</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                        <!-- Feature Item Start -->
                        <div class="col-md-4 col-sm-6 feature-item">
                            <div class="feature-item-icon">
                                <img src="<?php echo base_url()?>assets/img/features-img/06.png" alt="" class="img-responsive">
                            </div>
                            <div class="feature-item-content">
                                <h4>Technical Service</h4>
                                <p>Lorem ipsum dolor sit amet elite, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <!-- Feature Item End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features Area End -->
    
    <!-- Single Features Area Start -->
    <div id="singleFeatures">
        <div class="single-feature single-feature-1" data-bg-src="<?php echo base_url()?>assets/img/background-img/single-feature-1-bg.png">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 single-feature--image">
                        <img src="<?php echo base_url()?>assets/img/features-img/drag-and-drop.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-7 single-feature--content">
                        <h2>Drag and Drop Website Builder</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam cum delectus, sunt eos esse quasi dignissimos. Minima deserunt nobis molestias voluptatum ut sunt sapiente quia? Totam consequatur, excepturi error sint.</p>
                        <a href="#" class="btn btn-custom-reverse">Read More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-feature single-feature-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 single-feature--content">
                        <h2>Get Started Quickly &amp; Easily</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam cum delectus, sunt eos esse quasi dignissimos. Minima deserunt nobis molestias voluptatum ut sunt sapiente quia? Totam consequatur, excepturi error sint.</p>
                        <a href="#" class="btn btn-custom-reverse">Read More</a>
                    </div>
                    <div class="col-md-5 single-feature--image">
                        <img src="<?php echo base_url()?>assets/img/features-img/softaculous.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single Features Area End -->
    
    <!-- Counter Area Start -->
    <div class="counter" data-bg-src="<?php echo base_url()?>assets/img/background-img/counter-bg.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <!-- Counter Item Start -->
                    <div class="counter-holder">
                        <div class="counter-icon"><img src="<?php echo base_url()?>assets/img/counter-img/01.jpg" alt="" class="img-responsive"></div>
                        <p class="counter-text">cloud servers sold</p>
                        <div class="counter-number">400</div>
                    </div>
                    <!-- Counter Item End -->
                </div>
                <div class="col-sm-3 col-xs-6">
                    <!-- Counter Item Start -->
                    <div class="counter-holder">
                        <div class="counter-icon"><img src="<?php echo base_url()?>assets/img/counter-img/02.jpg" alt="" class="img-responsive"></div>
                        <p class="counter-text">dedicated servers sold</p>
                        <div class="counter-number">2,500</div>
                    </div>
                    <!-- Counter Item End -->
                </div>
                <div class="col-sm-3 col-xs-6">
                    <!-- Counter Item Start -->
                    <div class="counter-holder">
                        <div class="counter-icon"><img src="<?php echo base_url()?>assets/img/counter-img/03.jpg" alt="" class="img-responsive"></div>
                        <p class="counter-text">shared hosting sold</p>
                        <div class="counter-number">9,815</div>
                    </div>
                    <!-- Counter Item End -->
                </div>
                <div class="col-sm-3 col-xs-6">
                    <!-- Counter Item Start -->
                    <div class="counter-holder">
                        <div class="counter-icon"><img src="<?php echo base_url()?>assets/img/counter-img/04.jpg" alt="" class="img-responsive"></div>
                        <p class="counter-text">happy clients</p>
                        <div class="counter-number">8,815</div>
                    </div>
                    <!-- Counter Item End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Counter Area End -->
    
    <!-- Pricing Area Start -->
    <div id="pricing">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section-title">
                <h2>Populer Pricing</h2>
            </div>
            <!-- Section Title End -->
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <!-- Pricing Table Item Start -->
                    <div class="pricing-table-item">
                        <div class="pt-head">
                            <div class="caption">Starting At</div>
                            <div class="pt-price-tag">$9.99<span>/m</span></div>
                            <div class="pt-plan">shared hosting</div>
                        </div>
                        <div class="pt-body">
                            <div class="pt-features">
                                <ul>
                                    <li>2GB RAM</li>
                                    <li>20GB SSD Cloud Storage</li>
                                    <li>Weekly Backups</li>
                                    <li>DDoS Protection</li>
                                    <li>Full Root Access</li>
                                    <li>24/7/365 Tech Support</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pt-footer">
                            <a href="#" class="btn btn-custom-reverse">Buy Now</a>
                        </div>
                    </div>
                    <!-- Pricing Table Item End -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <!-- Pricing Table Item Start -->
                    <div class="pricing-table-item">
                        <div class="pt-head">
                            <div class="caption">Starting At</div>
                            <div class="pt-price-tag">$29.99<span>/m</span></div>
                            <div class="pt-plan">reseller hosting</div>
                        </div>
                        <div class="pt-body">
                            <div class="pt-features">
                                <ul>
                                    <li>4GB RAM</li>
                                    <li>50GB SSD Cloud Storage</li>
                                    <li>Weekly Backups</li>
                                    <li>DDoS Protection</li>
                                    <li>Full Root Access</li>
                                    <li>24/7/365 Tech Support</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pt-footer">
                            <a href="#" class="btn btn-custom-reverse">Buy Now</a>
                        </div>
                    </div>
                    <!-- Pricing Table Item End -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <!-- Pricing Table Item Start -->
                    <div class="pricing-table-item">
                        <div class="pt-head">
                            <div class="caption">Starting At</div>
                            <div class="pt-price-tag">$49.99<span>/m</span></div>
                            <div class="pt-plan">cloud vps</div>
                        </div>
                        <div class="pt-body">
                            <div class="pt-features">
                                <ul>
                                    <li>8GB RAM</li>
                                    <li>80GB SSD Cloud Storage</li>
                                    <li>Weekly Backups</li>
                                    <li>DDoS Protection</li>
                                    <li>Full Root Access</li>
                                    <li>24/7/365 Tech Support</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pt-footer">
                            <a href="#" class="btn btn-custom-reverse">Buy Now</a>
                        </div>
                    </div>
                    <!-- Pricing Table Item End -->
                </div>
                <div class="col-md-3 col-sm-6">
                    <!-- Pricing Table Item Start -->
                    <div class="pricing-table-item">
                        <div class="pt-head">
                            <div class="caption">Starting At</div>
                            <div class="pt-price-tag">$99.99<span>/m</span></div>
                            <div class="pt-plan">dedicated servers</div>
                        </div>
                        <div class="pt-body">
                            <div class="pt-features">
                                <ul>
                                    <li>16GB RAM</li>
                                    <li>100GB SSD Cloud Storage</li>
                                    <li>Weekly Backups</li>
                                    <li>DDoS Protection</li>
                                    <li>Full Root Access</li>
                                    <li>24/7/365 Tech Support</li>
                                </ul>
                            </div>
                        </div>
                        <div class="pt-footer">
                            <a href="#" class="btn btn-custom-reverse">Buy Now</a>
                        </div>
                    </div>
                    <!-- Pricing Table Item End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Pricing Area End -->
    
    <!-- Certificate Area Start -->
    <div id="certificate">
        <div class="container">
            <h2 class="h1">100% Powered by Certified Green Renewable Energy Sources</h2>
            <div class="certificate--img">
                <img src="https://www.hostpapa.eu/assets/green/bef.jpg" alt="">
                <img src="https://www.hostpapa.eu/assets/green/greentags.jpg" alt="">
            </div>
        </div>
    </div>
    <!-- Certificate Area Start -->
    
    <!-- Testimonial Area Start -->
    <div id="testimonial" data-bg-src="<?php echo base_url()?>assets/img/background-img/testimonial-bg.jpg">
        <div class="section-title">
            <h2>testimonial</h2>
        </div>
        <div class="testimonial-slider">
            <!-- Testimonial Item Start -->
            <div class="testimonial-item" data-recommender-thumb="<?php echo base_url()?>assets/img/testimonial-img/01.jpg">
                <div class="recommender-comment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum similique ad, magnam, odit repellat reprehenderit. Consequatur consectetur aspernatur ad assumenda a. Aspernatur fugit numquam quod rerum sint facere ex ullam. A, blanditiis quod, tempore magni veniam perferendis aliquid vitae saepe.</p>
                </div>
                <div class="recommender-info">
                    <span class="recommender-name">Mohammad Al Omayer,</span>
                    <span class="recommender-role">Company</span>
                </div>
            </div>
            <!-- Testimonial Item End -->
            <!-- Testimonial Item Start -->
            <div class="testimonial-item" data-recommender-thumb="<?php echo base_url()?>assets/img/testimonial-img/02.jpg">
                <div class="recommender-comment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum similique ad, magnam, odit repellat reprehenderit. Consequatur consectetur aspernatur ad assumenda a. Aspernatur fugit numquam quod rerum sint facere ex ullam. A, blanditiis quod, tempore magni veniam perferendis aliquid vitae saepe.</p>
                </div>
                <div class="recommender-info">
                    <span class="recommender-name">Mohammad Al Omayer,</span>
                    <span class="recommender-role">Company</span>
                </div>
            </div>
            <!-- Testimonial Item End -->
            <!-- Testimonial Item Start -->
            <div class="testimonial-item" data-recommender-thumb="<?php echo base_url()?>assets/img/testimonial-img/03.jpg">
                <div class="recommender-comment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum similique ad, magnam, odit repellat reprehenderit. Consequatur consectetur aspernatur ad assumenda a. Aspernatur fugit numquam quod rerum sint facere ex ullam. A, blanditiis quod, tempore magni veniam perferendis aliquid vitae saepe.</p>
                </div>
                <div class="recommender-info">
                    <span class="recommender-name">Mohammad Al Omayer,</span>
                    <span class="recommender-role">Company</span>
                </div>
            </div>
            <!-- Testimonial Item End -->
        </div>
    </div>
    <!-- Testimonial Area End -->
   
    <!-- Datacenter Locations Area Start -->
    <div id="datacenterLocations">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section-title">
                <h2>our Datacenter</h2>
            </div>
            <!-- Section Title End -->
            <div class="datacenter-locations">
                <img src="<?php echo base_url()?>assets/img/world-map.svg" alt="" class="img-responsive">
                <div class="datacenter-location-marker marker-1">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="Phoenix, USA"></i>
                </div>
                <div class="datacenter-location-marker marker-2">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="Lansing, USA"></i>
                </div>
                <div class="datacenter-location-marker marker-3">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="London, UK"></i>
                </div>
                <div class="datacenter-location-marker marker-4">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="New&nbsp;Delhi, India"></i>
                </div>
                <div class="datacenter-location-marker marker-5">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="Singapore"></i>
                </div>
                <div class="datacenter-location-marker marker-6">
                    <i class="fa fa-map-marker" data-toggle="tooltip" data-placement="top" title="Germany"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- Datacenter Locations Area End -->
    
    <!-- Brands Area Start -->
    <div id="brands">
        <div class="container">
            <div class="row brands-slider reset-margin">
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/01.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/02.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/03.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/04.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/05.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
                <!-- Brands Item Start -->
                <div class="col-md-12">
                    <img src="<?php echo base_url()?>assets/img/brands-img/06.png" alt="" class="img-responsive">
                </div>
                <!-- Brands Item End -->
            </div>
        </div>
    </div>
    <!-- Brands Area End -->
