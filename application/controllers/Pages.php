<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');    
    }
    
    public function index()
    {
        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }
    
    public function login()
    {
        $this->load->view('header');
        $this->load->view('pages/login');
        $this->load->view('footer');
    }
}
